const config = require('./config');
const cn = require('./amqp/consumer');
const args = process.argv.slice(2);

if (args.length > 0) {
  console.log("args[0]: " + args[0]);
}

for (let exchange of Object.values(config.ex_map.node2)) {
  cn.consume(exchange, 'Node2');
};