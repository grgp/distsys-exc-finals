#!/usr/bin/env node

const config = require('./config');
const d = new Date();
const pb = require('./amqp/publisher');
const args = process.argv.slice(2);
const send_from = args[0];
const send_to = args[1];

var data = {
  action: args[2].toLowerCase(),
  sender_id: send_from,
  type: 'request',
  ts: d.toISOString().replace(/\..*/, '').replace(/T/, ' ')
}

if (data.action == 'register' && args.length >= 3) {
  pb.publish(data, send_to);
} else {
  console.log('Call this interface by this format:');
  console.log('node interface.js <send_from> <send_to> <message/action>');
  console.log('\nExamples:');
  console.log('node interface.js node1 node2 message');
}

setTimeout(() => { process.exit(0) }, 1500);
